// import { useEffect, useState } from "react";
// import { Card, Col, Row, Container } from "react-bootstrap";
import { useEffect, useState, useContext } from "react";
import { Tab, Tabs, Col, Row, Form } from "react-bootstrap";
import { useLocation } from "react-router-dom";
import UserContext from "../UserContext";
import "../css/cartWishListOrders.css";
import { BsTrash } from "react-icons/bs";
import { AiOutlinePlus, AiOutlineMinus } from "react-icons/ai";
import Swal from "sweetalert2";
export default function MyProduct() {
  const { user } = useContext(UserContext);
  const location = useLocation();
  const keyInitial = location.state?.section;
  const [key, setKey] = useState(keyInitial);
  const [userCart, setUserCart] = useState(null);
  const [checkedState, setCheckedState] = useState(
    new Array(user.cart.length).fill(false)
  );
  let [productQty, setProductQty] = useState(
    user.cart.map((item) => item.quantity)
  );
  const [total, setTotal] = useState(0);
  const [checkAllState, setCheckAllState] = useState(false);
  const [shippingFee, setShippingFee] = useState(0);

  const handleCheckAll = (event) => {
    if (event.target.checked) {
      console.log("I Checked all");
      setCheckAllState(!checkAllState);
      handleOnChange(true);
    } else {
      console.log("I unchecked all");
      setCheckAllState(!checkAllState);
      handleOnChange(false);
    }
  };

  useEffect(() => {
    getProductCartDetails(false);
  }, [user]);

  useEffect(() => {
    if (checkedState.includes(true)) {
      handleOnChange();
    }
  }, [userCart]);

  const getProductCartDetails = async (checkOut) => {
    await fetch(`${process.env.REACT_APP_API_URL}/users/viewCart`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          setUserCart(data);
          setShippingFee(0);
          setTotal(0);
        } else {
          setUserCart(null);
        }
        if (checkOut === true && data[0] != undefined) {
          setCheckedState(new Array(data.cart.length).fill(false));
          setProductQty(data.cart.map((item) => item.quantity));
        }
      });
  };

  const checkOutFromCart = () => {
    const reqBody = [];
    for (let i = 0; i < checkedState.length; i++) {
      if (checkedState[i] === true) {
        reqBody.push({
          productId: userCart[i].productId,
          productName: userCart[i].name,
        });
      }
    }

    if (reqBody !== []) {
      fetch(`${process.env.REACT_APP_API_URL}/users/checkOutfromCart`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
        body: JSON.stringify(reqBody),
      })
        .then((res) => res.json())
        .then((data) => {
          if (data === true) {
            getProductCartDetails(true);
            Swal.fire({
              title: "Order placed!",
              icon: "success",
              text: "Your order is on its way!",
            });
          } else {
            Swal.fire({
              title: "Something went wrong",
              icon: "error",
              text: "Please try again.",
            });
          }
        });
    }
  };
  const deleteProductFromCart = async (productId, position) => {
    Swal.fire({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, remove it!",
    }).then((result) => {
      if (result.isConfirmed) {
        setCheckedState(checkedState.splice(position, 1));
        setProductQty(productQty.splice(position, 1));
        fetch(
          `${process.env.REACT_APP_API_URL}/users/removeFromCart/${productId}`,
          {
            method: "DELETE",
            headers: {
              "Content-Type": "application/json",
              Authorization: `Bearer ${localStorage.getItem("token")}`,
            },
          }
        )
          .then((res) => res.json())
          .then((data) => {
            if (data) {
              getProductCartDetails(false);
            }
          });
        Swal.fire(
          "Deleted!",
          "The product has been removed from cart.",
          "success"
        );
      }
    });
  };

  async function updateQtyFromInput(quantity, position) {
    if (quantity > 0 && quantity <= userCart[position].supply) {
      let updateProductQty = productQty.map((qty, index) =>
        index === position ? quantity : qty
      );
      setProductQty(updateProductQty);
      await changeProductQuantity(
        userCart[position].productId,
        updateProductQty[position],
        position
      );
      getProductCartDetails(false);
    }
  }

  async function quantityIncrement(position) {
    if (productQty[position] < userCart[position].supply) {
      let updateProductQty = productQty.map((qty, index) =>
        index === position ? qty + 1 : qty
      );
      setProductQty(updateProductQty);
      await changeProductQuantity(
        userCart[position].productId,
        updateProductQty[position],
        position
      );
      getProductCartDetails(false);
    }
  }

  async function quantityDecrement(position) {
    if (productQty[position] > 1) {
      let updateProductQty = productQty.map((qty, index) =>
        index === position ? qty - 1 : qty
      );
      setProductQty(updateProductQty);
      await changeProductQuantity(
        userCart[position].productId,
        updateProductQty[position],
        position
      );
      getProductCartDetails(false);
      // if (checkedState[position] === true) {
      //   handleOnChange();
      // }
    }
  }

  async function changeProductQuantity(productId, quantity, index) {
    if (productId && quantity) {
      // console.log({ productId: productId, quantity: quantity });
      await fetch(`${process.env.REACT_APP_API_URL}/users/changeProductQty`, {
        method: "PATCH",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
        body: JSON.stringify({
          productId: productId,
          quantity: quantity,
        }),
      })
        .then((res) => res.json())
        .then((data) => {
          if (!data) {
            alert("Something went wrong, cannot update the quantity.");
          }
        });
    }
  }

  const handleOnChange = (position) => {
    if (userCart[0] != undefined) {
      let updatedCheckedState = [];
      if (typeof position === "number") {
        updatedCheckedState = checkedState.map((item, index) =>
          index === position ? !item : item
        );
        setCheckedState(updatedCheckedState);
      } else if (typeof position === "boolean") {
        setCheckedState(new Array(user.cart.length).fill(position));
        updatedCheckedState = new Array(user.cart.length).fill(position);
      } else {
        updatedCheckedState = checkedState;
        // console.log(updatedCheckedState);
      }

      const totalPrice = updatedCheckedState.reduce(
        (sum, currentState, index) => {
          if (currentState === true) {
            console.log(userCart[index]);
            return sum + userCart[index].price * userCart[index].quantity;
          }
          return sum;
        },
        0
      );
      const shippingFee =
        updatedCheckedState.filter((value) => {
          return value === true;
        }).length * 100;
      setShippingFee(shippingFee);
      setTotal(totalPrice);
    }

    // console.log(shippingFee);
  };

  useEffect(() => {
    setKey(keyInitial);
  }, [keyInitial]);

  // function loadCart() {}

  return (
    <Tabs
      id="controlled-tab-example"
      activeKey={key}
      onSelect={(k) => setKey(k)}
      className="mt-2"
      justify
    >
      <Tab eventKey="cart" title="My Cart" className="ml-3 mr-3">
        {key == "cart" && userCart !== null && user.firstName != undefined ? (
          <div className="d-md-flex container">
            <Col className="col-xs-1 col-md-6 container">
              <Row>
                <div className="mt-3 mb-3">
                  <Form.Check
                    checked={checkAllState}
                    className="ml-2 mr-5"
                    onChange={handleCheckAll}
                    label={`  Select All (${userCart.length}) Item/s`}
                  />
                </div>
                {/* {console.log(userCart)} */}
                {userCart.map((item, index) => (
                  <div className="d-flex" key={index}>
                    <Form.Check
                      checked={checkedState[index]}
                      onChange={() => handleOnChange(index)}
                    />
                    <div>
                      <img src={item.image} className="cart-img" />
                    </div>
                    <div>
                      <p>{item.name}</p>
                      <p>{item.description}</p>
                    </div>
                    <div>
                      <p>
                        Php{" "}
                        {item.price.toLocaleString("en", {
                          minimumFractionDigits: 2,
                        })}
                      </p>
                      <BsTrash
                        onClick={() =>
                          deleteProductFromCart(item.productId, index)
                        }
                      />
                    </div>
                    <div>
                      <p>Quantity</p>
                      {/* <p>{item.quantity.toLocaleString("en")}</p> */}
                      <div className="cart-qty">
                        <div className="cart-qty-control">
                          <span
                            className="cart-qty-operation"
                            onClick={() => quantityDecrement(index)}
                          >
                            <AiOutlineMinus />
                          </span>
                          <input
                            className="cart-qty-input"
                            value={productQty[index]}
                            onChange={(e) =>
                              updateQtyFromInput(Number(e.target.value), index)
                            }
                          />

                          <span
                            className="cart-qty-operation"
                            onClick={() => quantityIncrement(index)}
                          >
                            <AiOutlinePlus />
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                ))}
              </Row>

              {userCart.length == 0 ? null : userCart.map((item) => {})}
            </Col>

            <Col className="col-xs-1 col-md-4">
              <div>
                <h6 className="mt-5">Order Summary</h6>

                <div className="position-relative">
                  <span>Subtotal: </span>
                  <span className="position-absolute end-0">
                    Php{" "}
                    {total.toLocaleString("en", {
                      minimumFractionDigits: 2,
                    })}
                  </span>
                </div>

                <div className="position-relative">
                  <span>Shipping fee: </span>
                  <span className="position-absolute end-0">
                    Php{" "}
                    {shippingFee.toLocaleString("en", {
                      minimumFractionDigits: 2,
                    })}
                  </span>
                </div>

                <hr />
                <div className="position-relative">
                  <span>Total: </span>
                  <span className="position-absolute end-0">
                    Php{" "}
                    {(shippingFee + total).toLocaleString("en", {
                      minimumFractionDigits: 2,
                    })}
                  </span>
                </div>
              </div>
              <button
                className="cart-btn-submit"
                // disabled={isDisabled}
                onClick={() => checkOutFromCart()}
              >
                Proceed to Checkout
              </button>
            </Col>
          </div>
        ) : null}
      </Tab>

      <Tab eventKey="wishlist" title="My Wishlist"></Tab>

      <Tab eventKey="orders" title="My Orders"></Tab>
    </Tabs>
  );
}
