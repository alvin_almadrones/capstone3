import { useEffect, useState } from "react";
import { Card, Col, Row } from "react-bootstrap";
import "../css/shop.css";

export default function Shop() {
  const [activeProducts, setActiveProducts] = useState();

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/allActiveProducts`)
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          setActiveProducts(
            <Row xs={1} md={3} className="g-4">
              {data.map((product, index) => {
                return (
                  <Col key={index}>
                    <a
                      href={`/productView/${product._id}`}
                      className="shop-anchor-link"
                    >
                      <Card>
                        <Card.Img
                          className="shop-card-img"
                          variant="top"
                          src={product.image.url}
                        />
                        <Card.Body className="shop-card-body">
                          <Card.Title>{product.name}</Card.Title>
                          <Card.Text>{product.description}</Card.Text>
                          <Card.Text>
                            Php {product.price.toLocaleString("en")}
                          </Card.Text>
                        </Card.Body>
                      </Card>
                    </a>
                  </Col>
                );
              })}
            </Row>
          );
        } else {
          setActiveProducts([]);
        }
      });
  });

  return <div className=" m-5">{activeProducts}</div>;
}
