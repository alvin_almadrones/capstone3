import { useEffect, useState, useContext } from "react";
import { useParams, Link } from "react-router-dom";

import "../css/productView.css";
import { Container, Button, Modal } from "react-bootstrap";
import {
  AiOutlinePlus,
  AiOutlineMinus,
  AiOutlineShoppingCart,
  AiOutlineHeart,
} from "react-icons/ai";
import UserContext from "../UserContext";
import Swal from "sweetalert2";
export default function ProductView() {
  const { user, setUser } = useContext(UserContext);
  const { productId } = useParams();
  const [product, setProduct] = useState([]);
  const [show, setShow] = useState(false);
  const [directToLogin, setDirectToLogin] = useState(false);
  const [directToCheckOut, setDirectToCheckOut] = useState(false);
  const handleClose = () => setShow(false);

  //console.log(user);
  let [productQty, setProductQty] = useState(1);

  function incrementQty() {
    if (productQty < product.supply) {
      productQty += 1;
      product.quantity = productQty;
    }
    setProductQty(productQty);
    setProduct(product);
  }

  function decrementQty() {
    if (productQty > 1) {
      productQty -= 1;
      product.quantity = productQty;
    }
    setProductQty(productQty);
    setProduct(product);
  }

  function addToCart(prodQty) {
    fetch(`${process.env.REACT_APP_API_URL}/users/addToCart`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        productId: product._id,
        productName: product.name,
        quantity: prodQty,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data === true) {
          fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            headers: {
              Authorization: `Bearer ${localStorage.getItem("token")}`,
            },
          })
            .then((res) => res.json())
            .then((userData) => {
              console.log(userData);
              setUser(userData);
            });

          Swal.fire({
            title: "Successfully added to cart!",
            icon: "success",
            text: "Your cart is waiving.",
          });
        } else {
          Swal.fire({
            title: "Something went wrong.",
            icon: "error",
            text: "Please try again.",
          });
        }
      });
  }

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
      .then((res) => res.json())
      .then((data) => {
        data.quantity = productQty;
        setProduct(data);
      });
  }, [productId]);

  return product.name != undefined ? (
    <Container className="d-flex justify-content-center mt-5">
      <div className="row col-md-10 col-xs-6">
        <div className="col-md-6 d-flex justify-content-center">
          <img className="img-fluid" src={product.image.url} />
        </div>

        <div className="pView-controls col-md-5">
          {console.log(product)}
          <h3>{product.name}</h3>
          <p>{product.description}</p>
          <p className="pView-price">
            Php{" "}
            {product.price.toLocaleString("en", { minimumFractionDigits: 2 })}
          </p>
          <p>Available Stock: {product.supply}</p>
          <div className="mt-5">
            <p>Quantity</p>
            <div className="pView-qty position-relative">
              <div className="pView-qty-control">
                <span className="pView-qty-operation" onClick={decrementQty}>
                  <AiOutlineMinus />
                </span>
                <input
                  className="pView-qty-input"
                  value={productQty}
                  onChange={(e) => {
                    setProductQty(Number(e.target.value));
                  }}
                />

                <span className="pView-qty-operation" onClick={incrementQty}>
                  <AiOutlinePlus />
                </span>
              </div>
              <span className="position-absolute end-0 top-0">
                <button className="pView-btn-icon">
                  <AiOutlineShoppingCart
                    className="pView-icons"
                    onClick={() => {
                      user._id ? addToCart(productQty) : setShow(true);
                    }}
                  />
                </button>
                <button className="pView-btn-icon">
                  <AiOutlineHeart
                    onClick={() => setShow(true)}
                    className="pView-icons"
                  />
                </button>
              </span>
            </div>
          </div>

          <Link
            // onClick={() =>
            //   user._id !== null ? setDirectToCheckOut(true) : setShow(true)
            //}

            to={{ pathname: user._id != undefined ? "/checkout" : "" }}
            onClick={() => {
              if (user._id == undefined) {
                setShow(true);
              }
            }}
            state={{ product: [product] }}
            className="btn pView-btn-submit"
          >
            Buy Now
          </Link>
        </div>
      </div>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Welcome to Kbuild!</Modal.Title>
        </Modal.Header>
        <Modal.Body>Please log in to continue.</Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button
            type="button"
            variant="primary"
            onClick={() => {
              handleClose();
              setDirectToLogin(true);
            }}
          >
            Ok
          </Button>
        </Modal.Footer>
      </Modal>
    </Container>
  ) : null;
}
