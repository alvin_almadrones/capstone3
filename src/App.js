import "./App.css";
import AppNavBar from "./components/AppNavBar";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { Container } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import { useEffect, useState } from "react";
import { UserProvider } from "./UserContext";
import Home from "./pages/Home";
import Logout from "./pages/Logout";
import LoginRegister from "./pages/Login-Register";
import Dashboard from "./pages/Dashboard";
import ProductView from "./pages/ProductView";
import Shop from "./pages/Shop";
import Checkout from "./pages/Checkout";
import MyProducts from "./pages/MyProducts";
function App() {
  const [user, setUser] = useState({});

  useEffect(() => {
    if (localStorage.getItem("token") !== null && user._id === undefined) {
      fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      })
        .then((res) => res.json())
        .then((data) => {
          setUser(data);
        });
    }
  }, [user]);

  const unsetUser = () => {
    localStorage.clear();
  };

  return localStorage.getItem("token") !== null &&
    user._id === undefined ? null : (
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>
        <AppNavBar />
        <Container>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/login" element={<LoginRegister />} />
            <Route path="/logout" element={<Logout />} />
            <Route path="/dashboard" element={<Dashboard />} />
            <Route path="/productView/:productId" element={<ProductView />} />
            <Route path="/shop" element={<Shop />} />
            <Route path="/myProduct" element={<MyProducts />} />
            <Route
              path="/checkout"
              // state={{ product: "" }}
              element={<Checkout />}
            />
          </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
