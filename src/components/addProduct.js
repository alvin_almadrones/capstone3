import { useState, useEffect } from "react";
import { Form, Button } from "react-bootstrap";
import Swal from "sweetalert2";
import "../css/dashboard.css";
import SELECT from "react-select";
import {
  AiFillCloseCircle,
  AiOutlineFileImage,
  AiOutlineCloseCircle,
} from "react-icons/ai";

export default function AddProduct({ product, open, isEdit, onClose }) {
  let pName = "";
  let pDesc = "";
  let pPrice = "";
  let pSupply = "";
  let pCategory = [];
  let pImage = null;
  console.log(isEdit);
  if (product) {
    pName = product.name;
    pDesc = product.description;
    pPrice = product.price;
    pSupply = product.supply;
    pCategory = product.category;
    pImage = product.image;
  }

  let productCategoryData = [
    { value: "Concreting & Masonry", label: "Concreting & Masonry" },
    { value: "Rebars & G.I Wires", label: "Rebars & G.I Wires" },
    { value: "Steel", label: "Steel" },
    { value: "Roofing & Insulation", label: "Roofing & Insulation" },
    { value: "Waterproofing", label: "Waterproofing" },
    { value: "Wood Products", label: "Wood Products" },
  ];

  const [selectedImage, setSelectedImage] = useState(null);
  const [productName, setProductName] = useState(pName);
  const [productDescription, setProductDescription] = useState(pDesc);
  const [productPrice, setProductPrice] = useState(pPrice);
  const [productSupply, setProductSupply] = useState(pSupply);
  const [category, setCategory] = useState(pCategory);
  const [image, setImage] = useState(pImage);
  const [isActive, setIsActive] = useState(false);
  console.log(productName);
  const assignCategory = (e) => {
    const tempCategory = e.map((z) => z.label);
    setCategory(tempCategory);
    //console.log(tempCategory);
  };
  useEffect(() => {
    setProductName(pName);
    setProductDescription(pDesc);
    setProductPrice(pPrice);
    setProductSupply(pSupply);
    setCategory(pCategory);
    setImage(pImage);
  }, [pName, pDesc, pPrice, pSupply, pCategory, pImage]);

  useEffect(() => {
    handleChange();
  }, [
    category,
    image,
    productName,
    productDescription,
    productPrice,
    productSupply,
    category,
  ]);

  const handleImage = (e) => {
    e.preventDefault();
    const file = e.target.files[0];
    setFileToBase(file);
    // console.log(image);
  };

  const clearFields = () => {
    pName = "";
    pDesc = "";
    pPrice = "";
    pSupply = "";
    pCategory = "";
    pImage = "";
    setSelectedImage(null);
    onClose();
  };

  const reqBodyToUpdate = () => {
    let reqBody = {};
    if (productName !== pName) {
      reqBody.name = productName;
    }
    if (productDescription !== pDesc) {
      reqBody.description = productDescription;
    }
    if (productPrice !== pPrice) {
      reqBody.price = productPrice;
    }
    if (pCategory !== category) {
      reqBody.category = category;
    }
    if (pImage !== image) {
      reqBody.image = image;
    }
    console.log(reqBody);
    return reqBody;
  };
  const setFileToBase = (file) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onloadend = () => {
      setImage(reader.result);
    };
  };

  const handleChange = () => {
    if (
      productName !== "" &&
      productDescription !== "" &&
      category[0] !== undefined &&
      image &&
      productPrice !== "" &&
      productSupply !== ""
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  };

  const saveNewProduct = (e) => {
    e.preventDefault();
    fetch(`${process.env.REACT_APP_API_URL}/products/addProduct`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        name: productName,
        description: productDescription,
        category: category,
        price: productPrice,
        supply: productSupply,
        image: image,
      }),
    }).then((res) =>
      res.json().then((data) => {
        //console.log(data);
        if (data) {
          Swal.fire({
            title: "Product succesfully Added",
            icon: "success",
            text: `${productName} is now added`,
          });
          window.location.reload();
          clearFields();
        } else {
          Swal.fire({
            title: "Error!",
            icon: "error",
            text: `Something went wrong. Please try again later!`,
          });
        }
      })
    );
  };

  const updateProduct = async (e) => {
    e.preventDefault();
    const apiResponse = await fetch(
      `${process.env.REACT_APP_API_URL}/products/updateProduct/${product._id}`,
      {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
        body: JSON.stringify(reqBodyToUpdate()),
      }
    );

    if ((await apiResponse.clone().json()) == true) {
      Swal.fire({
        title: "Product succesfully updated",
        icon: "success",
        text: `${productName} is now added`,
      });
      window.location.reload();
      clearFields();
    } else {
      Swal.fire({
        title: "Error!",
        icon: "error",
        text: `Cannot Edit. Please try again later!`,
      });
    }
  };

  if (!open) return null;
  return (
    <div>
      <div
        onClick={(e) => e.stopPropagation()}
        className="col-lg-6 position-relative addProduct-container"
      >
        <AiOutlineCloseCircle
          onClick={() => clearFields()}
          className="mb-3 position-absolute end-0 addProduct-closeBtn"
        />

        <Form
          onSubmit={(e) => {
            !isEdit ? saveNewProduct(e) : updateProduct(e);
          }}
          className="mt-5"
        >
          <Form.Group className="mb-3" controlId="prodName">
            <Form.Control
              type="text"
              placeholder={"Product Name"}
              value={productName}
              onChange={(e) => setProductName(e.target.value)}
            />
          </Form.Group>

          <Form.Group className="mb-3" controlId="prodDescription">
            <Form.Control
              type="text"
              placeholder="Product Description"
              value={productDescription}
              onChange={(e) => setProductDescription(e.target.value)}
            />
          </Form.Group>

          <Form.Group className="mb-3" controlId="prodPrice">
            <Form.Control
              type="number"
              placeholder="Price"
              value={productPrice}
              onChange={(e) => setProductPrice(e.target.value)}
            />
          </Form.Group>

          <Form.Group className="mb-3" controlId="proSupply">
            <Form.Control
              type="number"
              placeholder="Supply"
              value={productSupply}
              onChange={(e) => setProductSupply(e.target.value)}
            />
          </Form.Group>

          <SELECT
            isMulti
            value={productCategoryData.filter((obj) =>
              category.includes(obj.label)
            )}
            options={productCategoryData}
            className="dashboard-category-input"
            onChange={assignCategory}
          ></SELECT>

          <Form.Group className="mt-5">
            {(selectedImage || image.url !== null) && (
              <div className="position-relative">
                <div className="d-flex justify-content-center p-2 dashboard-add-product-image">
                  <img
                    alt="not found"
                    width={"60%"}
                    src={
                      selectedImage
                        ? URL.createObjectURL(selectedImage)
                        : image.url
                    }
                  />
                  <br />
                </div>

                <AiFillCloseCircle
                  onClick={() => {
                    setSelectedImage(null);
                    setImage("");
                  }}
                  className="position-absolute end-0 top-0 dashboard-image-close"
                />
              </div>
            )}

            <br />
            <br />
            <label className="custom-file-upload">
              <input
                type="file"
                name="myImage"
                accept="image/png, image/gif, image/jpeg"
                onChange={(event) => {
                  //console.log(event.target.files[0]);
                  setSelectedImage(event.target.files[0]);
                  handleImage(event);
                }}
              />
              {/* <AiOutlineUpload /> */}
              <AiOutlineFileImage />
            </label>
          </Form.Group>

          <Button
            variant="primary"
            type="submit"
            className="mt-3 dashboard-btn-submit"
            disabled={!isActive}
          >
            Submit
          </Button>
        </Form>
      </div>
    </div>
  );
}
